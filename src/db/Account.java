package db;

public class Account {
    private int AdvertisementID;
    private String AdvTitle;
    private String AdvDetails;
    private String AdvDateTime;
    private float Price;
    private String UserID;
    private String ModeratorID;
    private String CategoryID;
    private String StatusID;

    public Account(String advTitle, String advDetails, String advDateTime,
                   float price, String userID, String moderatorID, String categoryID, String statusID) {
        AdvTitle = advTitle;
        AdvDetails = advDetails;
        AdvDateTime = advDateTime;
        Price = price;
        UserID = userID;
        ModeratorID = moderatorID;
        CategoryID = categoryID;
        StatusID = statusID;
    }

    public int getAdvertisementID() {
        return AdvertisementID;
    }

    public String getAdvTitle() {
        return AdvTitle;
    }

    public String getAdvDetails() {
        return AdvDetails;
    }

    public String getAdvDateTime() {
        return AdvDateTime;
    }

    public float getPrice() {
        return Price;
    }

    public String getUserID() {
        return UserID;
    }

    public String getModeratorID() {
        return ModeratorID;
    }

    public String getCategoryID() {
        return CategoryID;
    }

    public String getStatusID() {
        return StatusID;
    }

    Object[] toArray() {
        return new Object[]{AdvTitle, AdvDetails, AdvDateTime, Price, UserID, ModeratorID, CategoryID, StatusID};
    }

}
